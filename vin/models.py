from .app import db 
from flask.ext.login import UserMixin
from .app import login_manager

class Wine(db.Model):
	id  = db.Column(db.Integer, primary_key = True)
	name  = db.Column(db.String(100))
	category = db.Column(db.String(100))
	country = db.Column(db.String(100))
	image = db.Column(db.String(100))
	quantity = db.Column(db.Integer)
	region = db.Column(db.String(100))
	varietal = db.Column(db.String(100))
	vintage = db.Column(db.Integer)
	def __repr__(self):
		return "<Wine (%d) %s %s %s %s %a %s %s %a>" % (self.id,self.name,self.category,self.country,self.image,self.quantity,self.region,self.varietal, self.vintage)


def get_sample():
	return Wine.query.limit(50).all()

WINE_PER_PAGE = 60
def get_sample_page(page):
	return Wine.query.paginate(page, WINE_PER_PAGE, False)

def get_Wine(id):
	w = Wine.query.get(id)
	return w


def get_id_max():
	liste = Wine.query.all()
	i = 0
	for wine in liste:
		if wine.id > 1 :
			i = wine.id
	return i
	

# Vintage 
def get_all_wines_by_Vintage():
	return Wine.query.order_by(Wine.vintage.desc()).all()

def get_Vintage():
	listeVintage = []
	listeWines = []
	for w in get_all_wines_by_Vintage():
		if w.vintage not in listeVintage:
			listeVintage.append(w.vintage)
			listeWines.append(w)
	return listeVintage,listeWines

def get_Wine_by_Vintage(vintage,page):
	wines = Wine.query.filter_by(vintage = vintage).paginate(page,WINE_PER_PAGE,False)
	return wines

# Region
def get_all_wines_Region():
	return Wine.query.order_by(Wine.region).all()

def get_region():
	listeRegion = []
	listeWines= []
	for w in get_all_wines_Region():
		if w.region not in listeRegion:
			listeRegion.append(w.region)
			listeWines.append(w)
	return listeRegion,listeWines

def get_Wine_by_Region(region,page):
	wines = Wine.query.filter_by(region = region).paginate(page,WINE_PER_PAGE,False)
	return wines

# Category
def get_all_wines_Category():
	return Wine.query.order_by(Wine.category).all()

def get_category():
	listeCategory = []
	listeWines= []
	for w in get_all_wines_Category():
		if w.category not in listeCategory:
			listeCategory.append(w.category)
			listeWines.append(w)
	return listeCategory,listeWines

def get_Wine_by_Category(category,page):
	wines = Wine.query.filter_by(category = category).paginate(page,WINE_PER_PAGE,False)
	return wines

# Varietal
def get_all_wines_Varietal():
	return Wine.query.order_by(Wine.varietal).all()

def get_varietal():
	listeVarietal = []
	listeWines= []
	for w in get_all_wines_Varietal():
		if w.varietal not in listeVarietal:
			listeVarietal.append(w.varietal)
			listeWines.append(w)
	return listeVarietal,listeWines

def get_Wine_by_Varietal(varietal,page):
	wines = Wine.query.filter_by(varietal = varietal).paginate(page,WINE_PER_PAGE,False)
	return wines

class User(db.Model, UserMixin):
	username = db.Column(db.String(50), primary_key=True)
	password = db.Column(db.String(64))

	def get_id(self):
		return self.username

@login_manager.user_loader
def load_user(username):
        return User.query.get(username)



