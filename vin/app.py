from flask import Flask
from flask.ext.bootstrap import Bootstrap

app = Flask(__name__)
app.debug = True
Bootstrap(app)

from flask.ext.script import Manager
manager = Manager(app)


import os.path
def mkpath(p):
	return os.path.normpath( os.path.join(os.path.dirname(__file__),p))

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../myapp.db'))
db = SQLAlchemy(app)

app.config['SECRET_KEY']="037123e3-3664-4de5-a0ca-27a6ed07d2d9"

from flask.ext.login import LoginManager
login_manager = LoginManager(app)
login_manager.login_view = "login"
