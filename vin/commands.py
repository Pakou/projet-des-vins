from .app import manager, db

@manager.command
def loaddb(filename):
	'''Creates the tables and populates them with data.'''
	
	# Creation de toutes les tables
	db.create_all()
	
	# Chargement de notre jeu de donnees
	import yaml
	wines = yaml.load(open(filename))
	
	# import des modeles
	from .models import Wine
	
	# Creation de la table Wine
	for w in wines:
		o = Wine(name = w["name"],
				 country = w["country"],
				 category = w["category"],
				 image = w["image"],
				 quantity = w["quantity"],
				 region = w["region"],
				 varietal = w["varietal"],
				 vintage = w["vintage"])
		db.session.add(o)
	db.session.commit()

@manager.command
def syncdb():
	'''Creates all missing tables.'''
	db.create_all()
	
@manager.command
def newuser(username, passwd):
        '''Adds a new user.'''
        from .models import User
        from hashlib import sha256
        m = sha256()
        m.update(passwd.encode())
        u = User(username=username, password=m.hexdigest())
        db.session.add(u)
        db.session.commit()
