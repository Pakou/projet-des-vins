from .app import app,db
from flask import render_template, url_for, redirect, request
import yaml
from .models import *
from flask.ext.wtf import Form
from wtforms.validators import DataRequired
from flask.ext.login import login_user, current_user, logout_user, login_required, session
from wtforms import StringField,HiddenField,PasswordField
from hashlib import sha256
from wtforms.validators import DataRequired

data = yaml.load(open("data.yml"))
@app.route("/")
@app.route("/index")
@app.route("/index/<int:page>")
def home(page=1):
	s = "/index"
	return render_template("home.html", title="Vins",wines = get_sample_page(page), prev = s)

@app.route("/panier")
@login_required
def panier():
	return render_template("panier.html", title="Panier",panier=session[current_user.username])

@app.route("/vintage")
def vintage():
	return render_template("selection.html", title="Vins par année",selection = get_Vintage()[0],wines = get_Vintage()[1], type = "vintage")

@app.route("/region")
def region():
	return render_template("selection.html", title="Vins par région",selection = get_region()[0],wines = get_region()[1], type="region")

@app.route("/category")
def category():
	return render_template("selection.html", title="Vins par catégorie",selection = get_category()[0],wines = get_category()[1], type="category")

@app.route("/varietal")
def varietal():
	return render_template("selection.html", title="Vins par variété",selection = get_varietal()[0],wines = get_varietal()[1], type = "varietal")

class Panier(Form):
        qte = StringField('Quantité')

@app.route("/one_wine/<int:id>")
def one_wine(id):
        w = get_Wine(id)
        f = Panier()
        return render_template("one_wine.html", title = w.name, wine = w, form = f)

@app.route("/add_panier/<int:id>", methods=("GET","POST",))
def add_panier(id):
        if not current_user.is_authenticated():
                return redirect('login')
        f = Panier()
        w = get_Wine(id)
        qte = int(f.qte.data)
        for l in session[current_user.username]:
                if l[0]==w.id:
                        l[2] += qte
                        return redirect(url_for('panier'))
        session[current_user.username].append([w.id,w.name,qte,w.image])
        return redirect(url_for('panier'))
        
@app.route("/suppr_panier/<int:id>", methods=("GET","POST",))
def suppr_panier(id):
        if not current_user.is_authenticated():
                return redirect('login')
        f = Panier()
        w = get_Wine(id)
        qte = int(f.qte.data)
        for l in session[current_user.username]:
                if l[0]==w.id:
                        l[2] -= qte
                        return redirect(url_for('panier'))
        session[current_user.username].append([w.id,w.name,qte,w.image])
        return redirect(url_for('panier'))
	
@app.route("/wines_by_vintage/<int:vintage>")
@app.route("/wines_by_vintage/<int:vintage>/<int:page>")
def wine_by_vintage(vintage,page=1):
	w = get_Wine_by_Vintage(vintage,page)
	s = "/wines_by_vintage/"+(str)(vintage)
	return render_template("home.html", title = "Vin de "+(str)(vintage) , wines = w, prev = s)

@app.route("/wines_by_region/<string:region>")
@app.route("/wines_by_region/<string:region>/<int:page>")
def wine_by_region(region,page=1):
	w = get_Wine_by_Region(region,page)
	s = "/wines_by_region/"+region
	return render_template("home.html", title = "Vin de "+region , wines = w, prev = s)

@app.route("/wines_by_category/<string:category>")
@app.route("/wines_by_category/<string:category>/<int:page>")
def wine_by_category(category,page=1):
	w = get_Wine_by_Category(category,page)
	s = "/wines_by_category/"+category
	return render_template("home.html", title = category , wines = w,prev = s)
	
@app.route("/wines_by_varietal/<string:varietal>")
@app.route("/wines_by_varietal/<string:varietal>/<int:page>")
def wine_by_varietal(varietal,page=1):
	w = get_Wine_by_Varietal(varietal,page)
	s = "/wines_by_varietal/"+varietal
	return render_template("home.html", title = varietal , wines = w,prev = s)


# Login
class LoginForm(Form):
	username = StringField('Pseudo')
	password = StringField('Mot de passe')
	next = HiddenField()
	
	def get_authenticated_user(self):
		user = User.query.get(self.username.data)
		if user is None:
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None


@app.route("/login/", methods=("GET","POST",))
def login():
	f = LoginForm()
	if not f.is_submitted():
		f.next.data = request.args.get("next")
	elif f.validate_on_submit():
		user = f.get_authenticated_user()
		if user:
			login_user(user)
			#session[current_user.username] = []
			if current_user.username not in session:
				session[current_user.username] = []
			next = f.next.data or url_for("home")
			return redirect(next)
	return render_template("login.html",form = f)

@app.route("/logout")
def logout():
	logout_user()
	return redirect(url_for('home'))

# New user
@app.route("/add_user")
def add_user():
	u = None
	f = LoginForm(
				username = None,
				password = None,
				)
	return render_template("edit_user.html",
							title="Ajout d'un utilisateur",
							user = u,
							form = f
							)
@app.route("/save_user", methods=("POST",))
def save_user():
        u = None
        f = LoginForm()
        if f.validate_on_submit():
                password = f.password.data
                m = sha256()
                m.update(password.encode())
                passwd = m.hexdigest()
                u = User(username= f.username.data,
                         password = passwd)
                db.session.add(u)
                db.session.commit()
                form = LoginForm()
                return render_template("home.html", title="Vins",wines = get_sample_page(1))
        return render_template("edit_user.html",user=u, form = f)


# Wine
class WineForm(Form):
	id = HiddenField('id')
	name  = StringField('Nom :',validators=[DataRequired()])
	category = StringField('Catégorie :',validators=[DataRequired()])
	country = StringField('Pays :',validators=[DataRequired()])
	image = StringField('Nom de l\'image :',validators=[DataRequired()])
	quantity = StringField('Quantité :',validators=[DataRequired()])
	region = StringField('Région :',validators=[DataRequired()])
	varietal = StringField('Variété :',validators=[DataRequired()])
	vintage = StringField('Année :',validators=[DataRequired()])


@app.route("/edit_wine/<int:id>")
@login_required
def edit_wine(id):
	if current_user.is_authenticated() and current_user.username=="admin":
		w = get_Wine(id)
		f = WineForm(id = w.id, 
					name = w.name,
					category = w.category,
					country = w.country,
					image = w.image,
					quantity = w.quantity,
					region = w.region,
					varietal = w.varietal,
					vintage = w.vintage
					)
		return render_template("edit_wine.html",
								wine = w,
								form = f
								)
	return render_template("home.html", title="Vins",wines = get_sample_page(1))

@app.route("/delete_wine" , methods=("POST",))
@login_required
def delete_wine():
	if current_user.is_authenticated() and current_user.username=="admin":
		w = None
		f = WineForm()
		id = int(f.id.data)
		w = get_Wine(int(f.id.data))
		db.session.delete(w)
		db.session.commit()
		return render_template("home.html",title="Vins",wines = get_sample_page(1))
	return render_template("home.html", title="Vins",wines = get_sample_page(1))

@app.route("/add_wine")
@login_required
def add_wine():
	if current_user.is_authenticated() and current_user.username=="admin":
		w = None
		f = WineForm()
		return render_template("add_wine.html",
								wine = w,
								form = f
								)
	return render_template("home.html", title="Vins",wines = get_sample_page(1))
							
@app.route("/save_wine", methods=("POST",))
def save_wine():
	w = None
	f = WineForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		w = get_Wine(id) 
		w.name= f.name.data
		w.category= f.category.data
		w.country= f.country.data
		w.image= f.image.data
		w.quantity= f.quantity.data
		w.region= f.region.data
		w.varietal= f.varietal.data
		w.vintage= f.vintage.data
		db.session.commit()
		return redirect(url_for('one_wine',id=w.id))
	w = get_Wine(int(f.id.data))
	return render_template("edit_wine.html",wine=w, form = f)

@app.route("/new_wine", methods=("POST",))
def new_wine():
	w = None
	f = WineForm()
	image = f.image.data
	if image == "":
		image = "369750.jpg"
	w = Wine(
			id = get_id_max()+1,
			name= f.name.data,
			category= f.category.data,
			country= f.country.data,
			image= image,
			quantity= f.quantity.data,
			region= f.region.data,
			varietal= f.varietal.data,
			vintage= f.vintage.data,
		)
	db.session.add(w)
	db.session.commit()
	return redirect(url_for('one_wine',id=w.id))
